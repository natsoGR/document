<?php

namespace Natso\Document\Model\Checkout;

class PaymentInformationManagementPlugin
{
    protected $customerRepository;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
    ) {
        $this->customerRepository  = $customerRepositoryFactory->create();
    }

    public function aroundSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        // get JSON post data
        $request_body = file_get_contents('php://input');

        // decode JSON post data into array
        $data = json_decode($request_body, true);

        // run parent method and capture int $orderId
        $orderId = $proceed($cartId, $paymentMethod, $billingAddress);

        $customer = $this->customerRepository->getById($data['billingAddress']['customerId']);

        if (
            isset($data['paymentMethod']['additional_data']['document'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['document']))
        ) {
            $customer->setCustomAttribute(
                'document',
                $data['paymentMethod']['additional_data']['document']
            );
        }

        if (
            isset($data['paymentMethod']['additional_data']['vat_number'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['vat_number']))
        ) {
            $customer->setCustomAttribute(
                'vat_number',
                $data['paymentMethod']['additional_data']['vat_number']
            );
        }

        if (
            isset($data['paymentMethod']['additional_data']['company_name'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['company_name']))
        ) {
            $customer->setCustomAttribute(
                'company_name',
                $data['paymentMethod']['additional_data']['company_name']
            );
        }

        if (
            isset($data['paymentMethod']['additional_data']['company_address'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['company_address']))
        ) {
            $customer->setCustomAttribute(
                'company_address',
                $data['paymentMethod']['additional_data']['company_address']
            );
        }

        if (
            isset($data['paymentMethod']['additional_data']['job'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['job']))
        ) {
            $customer->setCustomAttribute(
                'job',
                $data['paymentMethod']['additional_data']['job']
            );
        }

        if (
            isset($data['paymentMethod']['additional_data']['tax_office'])
            &&
            !empty(isset($data['paymentMethod']['additional_data']['tax_office']))
        ) {
            $customer->setCustomAttribute(
                'tax_office',
                $data['paymentMethod']['additional_data']['tax_office']
            );
        }

        $this->customerRepository->save($customer);

        return $orderId;
    }
}