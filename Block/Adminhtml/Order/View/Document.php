<?php

namespace Natso\Document\Block\Adminhtml\Order\View;

class Document extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
	protected $customerRepository;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
        array $data = []
    ) {
        $this->customerRepository  	= $customerRepositoryFactory->create();
        parent::__construct($context, $registry, $adminHelper, $data);
    }

    public function aroundGetCustomerAccountData(\Magento\Sales\Block\Adminhtml\Order\View\Info $subject,\Closure $proceed)
    {
        $accountData = $proceed();
    	if (!empty($this->getOrder()->getCustomerId())) {
	    	$customer = $this->customerRepository->getById($this->getOrder()->getCustomerId());
	    	$customAttributes = $customer->getCustomAttributes();
            if (isset($customAttributes['document'])) {
                array_push(
                    $accountData,
                    array(
                        'label' => __('Document'),
                        'value' => ($customAttributes['document']->getValue()==1)?__('Receipt'):__('Invoice')
                        )
                    );

                if ($customAttributes['document']->getValue() == 2){
                    if (isset($customAttributes['vat_number'])){
                        array_push($accountData,array('label'=>__('Vat Number'), 'value' => $customAttributes['vat_number']->getValue()));
                    }
                    if (isset($customAttributes['company_name'])) {
                        array_push($accountData,array('label'=>__('Company Name'), 'value' => $customAttributes['company_name']->getValue()));
                    }
                    if (isset($customAttributes['company_address'])) {
                        array_push($accountData,array('label'=>__('Company Address'), 'value' => $customAttributes['company_address']->getValue()));
                    }
                    if (isset($customAttributes['job'])) {
                        array_push($accountData,array('label'=>__('Job'), 'value' => $customAttributes['job']->getValue()));
                    }
                    if (isset($customAttributes['tax_office'])) {
                        array_push($accountData,array('label'=>__('Tax Office'), 'value' => $customAttributes['tax_office']->getValue()));
                    }
                }
            }
    	}
        return $accountData;
    }
}