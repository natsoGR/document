define([
    'jquery',
    'Magento_Ui/js/form/element/select'
], function ($, Select) {
    'use strict';
    return Select.extend({
        defaults: {
            customName: '${ $.parentName }.${ $.index }_input'
        },
        /**
         * Change currently selected option
         * @param {String} id
         */
        selectOption: function(id){
            if(($("#"+id).val() == 1)) {
                $('[name="beforeMethods.company_name"]').hide();
                $('[name="beforeMethods.company_address"]').hide();
                $('[name="beforeMethods.vat_number"]').hide();
                $('[name="beforeMethods.job"]').hide();
                $('[name="beforeMethods.tax_office"]').hide();
            } else if($("#"+id).val() == 2) {
                $('[name="beforeMethods.company_name"]').show();
                $('[name="beforeMethods.company_address"]').show();
                $('[name="beforeMethods.vat_number"]').show();
                $('[name="beforeMethods.job"]').show();
                $('[name="beforeMethods.tax_office"]').show();
            }
        },
    });
});