<?php

namespace Natso\Document\Model\Customer\Attribute\Source;

class Document extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('Receipt'), 'value' => 1],
                ['label' => __('Invoice'), 'value' => 2],
            ];
        }
        return $this->_options;
    }
}
