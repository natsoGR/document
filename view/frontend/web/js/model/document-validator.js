define(
    ['jquery'],
    function ($) {
        'use strict';
        return {
            /**
             * Validate document inputs
             *
             * @returns {boolean}
             */
            validate: function() {
                if ($('select[name="document"]').length) {
                    var selected_document = $('select[name="document"]').val();
                    if (selected_document == '') {
                        return false;
                    }
                    if (selected_document == 2) {
                        var vat_number          = $('input[name="vat_number"]').val();
                        var company_name        = $('input[name="company_name"]').val();
                        var company_address     = $('input[name="company_address"]').val();
                        var job                 = $('input[name="job"]').val();
                        var tax_office          = $('input[name="tax_office"]').val();
                        if (vat_number == '' || company_name == '' || company_address == '' || job == '' || tax_office == ''){
                            return false;
                        }
                    }
                }
                return true;
            }
        }
    }
);