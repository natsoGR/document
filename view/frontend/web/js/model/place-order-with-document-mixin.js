define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {

    'use strict';

    return function (placeOrderAction) {

        return wrapper.wrap(placeOrderAction, function (originalAction, paymentData, redirectOnSuccess, messageContainer) {

            if (window.isCustomerLoggedIn) {

                var selected_document   = $('select[name="document"]').val();
                var vat_number          = $('input[name="vat_number"]').val();
                var company_name        = $('input[name="company_name"]').val();
                var company_address     = $('input[name="company_address"]').val();
                var job                 = $('input[name="job"]').val();
                var tax_office          = $('input[name="tax_office"]').val();

                paymentData.additional_data.document        = selected_document;
                paymentData.additional_data.vat_number      = vat_number;
                paymentData.additional_data.company_name    = company_name;
                paymentData.additional_data.company_address = company_address;
                paymentData.additional_data.job             = job;
                paymentData.additional_data.tax_office      = tax_office;

            }
            return originalAction(paymentData, redirectOnSuccess, messageContainer);
        });
    };
});
