<?php

namespace Natso\Document\Plugin\Checkout\Model\Checkout;

class LayoutProcessor
{
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
        \Magento\Customer\Model\Session $customerSession
    ){
        $this->customerSession      = $customerSession;
        $this->customerRepository   = $customerRepositoryFactory->create();
    }

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    )
    {
        if ($this->customerSession->getCustomerId() === null) {
            return $jsLayout;
        }

        $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['component'] = 'Natso_Document/js/view/checkout/review/document-block';

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['displayArea'] = 'beforeMethods';

        $document = '';
        $visible = false;
        if ( !empty( $customer->getCustomAttribute('document') ) ) {
        	$document = $customer->getCustomAttribute('document')->getValue();
            if ($document == 2) { //Invoice
                $visible = true;
            } elseif ($document == 1) { //Receipt
                $visible = false;
            }
        }

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['document'] = [
                'component' => 'Natso_Document/js/form/element/natso-select',
                'config' => [
                    'customScope'   => 'beforeMethods',
                    'template'      => 'ui/form/field',
                    'elementTmpl'   => 'Natso_Document/form/element/natso-select',
                    'id' => 'document',
                ],
                'dataScope' => 'beforeMethods.document',
                'label' => __('Invoice / Receipt'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'additionalClasses' => 'document-field',
                'validation' => [
                    'required-entry' => true
                ],
                'sortOrder' => 0,
                'id' => 'document',
                'value' => $document,
                'options' => [
                    [
                        'value' => '',
                        'label' => __('Please select document'),
                    ],
                    [
                        'value' => 1,
                        'label' => __('Receipt'),
                    ],
                    [
                        'value' => 2,
                        'label' => __('Invoice'),
                    ]
                ]
            ];

            $vatNumber = '';
            if ( $customer->getCustomAttribute('vat_number') != null ) {
                $vatNumber = $customer->getCustomAttribute('vat_number')->getValue();
            }

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['vat_number'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'beforeMethods',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'options' => [],
                    'id' => 'vat_number'
                ],
                'dataScope' => 'beforeMethods.vat_number',
                'label' => __('Vat Number'),
                'provider' => 'checkoutProvider',
                'visible' => $visible,
                'validation' => [
                    'required-entry' => true
                ],
                'additionalClasses' => 'document-field',
                'value' => $vatNumber,
                'sortOrder' => 1,
                'id' => 'vat_number'
            ];

            $companyName = '';
	        if ( $customer->getCustomAttribute('company_name') != null ) {
	        	$companyName = $customer->getCustomAttribute('company_name')->getValue();
	        }

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['company_name'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'beforeMethods',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'options' => [],
                    'id' => 'company_name'
                ],
                'dataScope' => 'beforeMethods.company_name',
                'label' => __('Company Name'),
                'provider' => 'checkoutProvider',
                'visible' => $visible,
                'additionalClasses' => 'document-field',
                'validation' => [
                    'required-entry' => true
                ],
                'value' => $companyName,
                'sortOrder' => 2,
                'id' => 'company_name'
            ];

            $companyAddress = '';
	        if ( $customer->getCustomAttribute('company_address') != null ) {
	        	$companyAddress = $customer->getCustomAttribute('company_address')->getValue();
	        }

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['company_address'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'beforeMethods',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'options' => [],
                    'id' => 'company_name'
                ],
                'dataScope' => 'beforeMethods.company_address',
                'label' => __('Company Address'),
                'provider' => 'checkoutProvider',
                'visible' => $visible,
                'additionalClasses' => 'document-field',
                'validation' => [
                    'required-entry' => true
                ],
                'value' => $companyAddress,
                'sortOrder' => 3,
                'id' => 'company_address'
            ];

            $companyAddress = '';
            if ( $customer->getCustomAttribute('job') != null ) {
                $companyAddress = $customer->getCustomAttribute('job')->getValue();
            }

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['job'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'beforeMethods',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'options' => [],
                    'id' => 'company_name'
                ],
                'dataScope' => 'beforeMethods.job',
                'label' => __('Job'),
                'provider' => 'checkoutProvider',
                'visible' => $visible,
                'additionalClasses' => 'document-field',
                'validation' => [
                    'required-entry' => true
                ],
                'value' => $companyAddress,
                'sortOrder' => 4,
                'id' => 'job'
            ];

            $companyAddress = '';
            if ( $customer->getCustomAttribute('tax_office') != null ) {
                $companyAddress = $customer->getCustomAttribute('tax_office')->getValue();
            }

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['documents']['children']['tax_office'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'beforeMethods',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'options' => [],
                    'id' => 'company_name'
                ],
                'dataScope' => 'beforeMethods.tax_office',
                'label' => __('Tax Office'),
                'provider' => 'checkoutProvider',
                'visible' => $visible,
                'additionalClasses' => 'document-field',
                'validation' => [
                    'required-entry' => true
                ],
                'value' => $companyAddress,
                'sortOrder' => 5,
                'id' => 'tax_office'
            ];

        return $jsLayout;
    }
}