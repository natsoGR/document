define(
    [
        'jquery',
        'ko',
        'uiComponent',
    ],
    function ($, ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Natso_Document/checkout/review/document-block'
            },
            initialize: function () {
                this._super(); //you must call super on components or they will not render
            }
        });
    }
);

